var gulp = require("gulp")
    , img = require("gulp-imagemin")
    , pug = require("gulp-pug")
    , sass = require("gulp-sass")
    , watch = require("gulp-watch")
    , rename = require("gulp-rename")
    , jsmin = require('gulp-jsmin')
    , del = require('del')
    , autoPrefix = require('gulp-autoprefixer')
    , rigger = require('gulp-rigger')
    , gutil = require('gulp-util')
    , sourcemaps = require('gulp-sourcemaps')
    , browserSync = require("browser-sync").create()
    , reload = browserSync.reload;

var config = require('./gulpconfig.json');

gulp.task('js:dev', function () {
    return gulp.src(config.dev.js)
        .pipe(rigger())
        .pipe(rename({suffix: ".min"}))
        .pipe(gulp.dest(config.prod.js));
});

gulp.task('js:prod', function () {
    return gulp.src(config.dev.js)
        .pipe(rigger())
        .pipe(jsmin())
        .pipe(rename({suffix: ".min"}))
        .pipe(gulp.dest(config.prod.js));
});

gulp.task('sass:dev', function () {
    return gulp.src(config.dev.style)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoPrefix())
        .pipe(rename({suffix: ".min"}))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(config.prod.css));
});

gulp.task('sass:prod', function () {
    return gulp.src(config.dev.style)
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoPrefix())
        .pipe(rename({suffix: ".min"}))
        .pipe(gulp.dest(config.prod.css));
});

gulp.task('html:dev', function (done) {
    return gulp.src(config.dev.html)
        .pipe(pug({pretty: true}))
        .on('error', function (err) {
            gutil.log(gutil.colors.red('[Error]'), err.toString());
        })
        .pipe(gulp.dest(config.prod.html));
});

gulp.task('html:prod', function (done) {
    return gulp.src(config.dev.html)
        .pipe(pug())
        .on('error', function (err) {
            gutil.log(gutil.colors.red('[Error]'), err.toString());
        })
        .pipe(gulp.dest(config.prod.html));
});

gulp.task('img:dev', function () {
    return gulp.src(config.dev.img)
        .pipe(gulp.dest(config.prod.img));
});

gulp.task('img:prod', function () {
    return gulp.src(config.dev.img)
        .pipe(img())
        .pipe(gulp.dest(config.prod.img));
});

gulp.task('fonts', function () {
    gulp.src(config.dev.fonts)
        .pipe(gulp.dest(config.prod.fonts))
});

gulp.task('server', function () {
    browserSync.init(config.server);
});

gulp.task('sass:watch', ['sass:dev'], function (done) {
    browserSync.reload();
    done();
});

gulp.task('html:watch', ['html:dev'], function (done) {
    browserSync.reload();
    done();
});

gulp.task('js:watch', ['js:dev'], function (done) {
    browserSync.reload();
    done();
});

gulp.task('img:watch', ['img:dev'], function (done) {
    browserSync.reload();
    done();
});

gulp.task('clean', function () {
    del.sync(config.clean);
});

gulp.task('watch', function () {
    gulp.watch(config.watch.style, ['sass:watch']);
    gulp.watch(config.watch.html, ['html:watch']);
    gulp.watch(config.watch.img, ['img:watch']);
    gulp.watch(config.watch.js, ['js:watch']);
    gulp.watch(config.watch.fonts, ['fonts']);
});

gulp.task('default', ['html:dev', 'img:dev', 'sass:dev','js:dev','fonts']);
gulp.task('build', ['html:prod', 'img:prod', 'sass:prod','js:prod','fonts']);

gulp.task('dev', ['server','default','watch']);
gulp.task('prod', ['clean','build']);






