GIT: git@bitbucket.org:alex_gusto/swiss-style.ru.git
##Run Node/PowerShell  cmd

###if you don't have gulp 
```
npm install -g gulp
```
###For first start project
```
npm install  - install all necessary plugins for project  
```
###For development 
```
gulp dev    - watching changes during development 
gulp clean  - clean prod/ directory
```
###For production 
```
gulp build - build project
gulp prod  - clean and build project 
```     