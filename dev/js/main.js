//= libs/slick.min.js
//= libs/jquery.elevatezoom.js

//= libs/jquery-ui/jquery-slider.js

//= libs/bootstrap/transition.js
//= libs/bootstrap/collapse.js
//= libs/bootstrap/tab.js
//= libs/bootstrap/modal.js
//= libs/bootstrap/dropdown.js


var COMMON_JS = window.COMMON_JS || {};

COMMON_JS.config = {
    isTouch: ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch,

    $addToCart: $(".btn--add-cart"),

    //Header
    $headerScroll: $("#headerScroll"),
    $header: $("#header"),
    $mainMenu: $("#mainMenu"),

    //Modals
    $discountModal: $("#discountModal"),
    $forgotPasswordModal: $("#forgotPasswordModal"),
    $registrationModal: $("#registrationModal"),
    $loginModal: $("#loginModal"),

    $overLayer: $("#overLayer"),

    //Primary menu
    $dropDown: $(".dropdown"),

    //Search 
    $searchForm: $("#searchForm"),
    $searchResult: $("#searchResult"),
    $topBarSearch: $("#topBarSearch"),
    $topBarSearchHandle: $("#topBarSearchHandle"),

    //Catalog
    $catalogProductsView: $("#catalogProductsView"),
    $catalogGrid: $("#catalogGrid"),
    productCard: ".product-card",
    iconList: "icon-list",
    iconTiles: "icon-tiles",
    iconPrice: "icon-price",

    //Inputs
    $priceRange: $("#priceRange"),

    // Buttons

    $buttonMoveUp: $('#moveUp'),

    //Slider's holder
    $specialsSlider: $("#specialsSlider"),
    $productThumbs: $("#productThumbs"),
    $featuredSlider: $("#featuredSlider"),

    //Product page
    $productImage: $("#productImage"),
    $productToBasket: $("#productToBasket"),
    $placeReview: $("#placeReview"),
    $productTabs: $("#productTabs")
}

COMMON_JS.init = function () {
    try {
        COMMON_JS.sliders();
        COMMON_JS.productImages();
        COMMON_JS.modals();
        COMMON_JS.catalogView();
        COMMON_JS.prodactTabs();
        COMMON_JS.search();
        COMMON_JS.jqueryUI();
        COMMON_JS.mobileMenu();
        COMMON_JS.buttonScrollUp();

        if (!COMMON_JS.config.isTouch) {
            COMMON_JS.menuDropDown();
        }

        $(window).on('resize load', function () {
            COMMON_JS.headerOnScroll();
        })
    }
    catch (error) {
        console.log(error);
    }
};

COMMON_JS.buttonScrollUp = function () {
    $buttonMoveUp = COMMON_JS.config.$buttonMoveUp;

    $(window).scroll(function () {
        if (window.pageYOffset > 300) {
            $buttonMoveUp.show();
        } else {
            $buttonMoveUp.hide();
        }
    });


    $buttonMoveUp.on('click', function () {
        $('html,body').animate({
            scrollTop: 0
        }, 700);
    });
}

COMMON_JS.headerOnScroll = function () {
    var $headerScroll = COMMON_JS.config.$headerScroll,
        $header = COMMON_JS.config.$header,
        $mainMenu = COMMON_JS.config.$mainMenu,
        windowOffsetTopPrev = 0;

    function hasScrolled() {
        var windowOffsetTop = window.pageYOffset;

        if (window.outerWidth > 767) {
            if (windowOffsetTop > 277) {
                $headerScroll.slideDown();
            } else {
                $headerScroll.slideUp();
            }
        } else if (!$(".menu-mobile .menu-primary__link").hasClass("open")) {
            if (windowOffsetTop > windowOffsetTopPrev && windowOffsetTop > 80) {
                $mainMenu.collapse("hide");
            } else {
                $mainMenu.collapse("show");
            }
            windowOffsetTopPrev = windowOffsetTop;
        }
    };

    $(window).scroll(function () {
        clearTimeout($.data(this, 'scrollTimer'));
        $.data(this, 'scrollTimer', setTimeout(function () {
            hasScrolled();
        }, 150));
    });
};

COMMON_JS.menuDropDown = function () {
    var $dropDown = COMMON_JS.config.$dropDown;

    $dropDown.on('mouseenter', function () {
        $(this).addClass("open");

    });

    $dropDown.on('mouseleave', function () {
        $dropDown.removeClass("open");
        //$header.css("position", "fixed");
    });

    $('.dropdown-toggle').on('click', function () {
        location.href = $(this).attr("href");
    });
}


COMMON_JS.jqueryUI = function () {
    var $priceRange = COMMON_JS.config.$priceRange
        , $maxPrice = $("[name='finish_price']")
        , $minPrice = $("[name='start_price']");

    $priceRange.slider({
        range: true,
        min: 0,
        max: $maxPrice.val(),
        values: [0, $maxPrice.val()],
        slide: function (event, ui) {
            $maxPrice.val(ui.values[1]);
            $minPrice.val(ui.values[0]);
        }
    });
}

COMMON_JS.mobileMenu = function () {
    var $header = COMMON_JS.config.$header;

    $('span.menu-mobile__link').on("click", function (e) {
        $(this).children().slideToggle();
        e.stopPropagation();
        console.log("link show");
    });


    $(".menu-mobile .dropdown").on("show.bs.dropdown", function () {
        $header.css("position", "absolute");
    });

    $(".menu-mobile .dropdown").on("hide.bs.dropdown", function () {
        $header.css("position", "fixed");
    });
}

COMMON_JS.search = function () {
    var $searchForm = COMMON_JS.config.$searchForm
        , $searchResult = COMMON_JS.config.$searchResult
        , $overLayer = COMMON_JS.config.$overLayer
        , $topBarSearch = COMMON_JS.config.$topBarSearch
        , $topBarSearchHandle = COMMON_JS.config.$topBarSearchHandle;

    $("input[type='search']").focus(function () {
        if ($(this).closest($searchForm).length) $searchForm.addClass("on-focus").parent().addClass("relative");
        $overLayer.show();
        $searchResult.slideDown();
    })

    $("input[type='search']").focusout(function () {
        if ($(this).closest($searchForm).length) {
            $searchForm.removeClass("on-focus");

            setTimeout(function () {
                $searchForm.parent().removeClass("relative");
            }, 300);
        }
        $searchResult.slideUp();
        $overLayer.delay(500).hide(0);
    })
}

COMMON_JS.prodactTabs = function () {
    var $placeReview = COMMON_JS.config.$placeReview
        , $productTabs = COMMON_JS.config.$productTabs
        , productTabsOffsetTop;

    if ($productTabs.length) productTabsOffsetTop = $productTabs.offset().top;

    $placeReview.click(function () {
        $("html, body").animate({ scrollTop: productTabsOffsetTop }, 1000);
        $productTabs.find("a[href='#tab5']").tab('show');
    })

}

// COMMON_JS.basket = {
//     add: function (productID, quantity) {
//         var $productToBasket = COMMON_JS.config.$productToBasket,
//             $modal = COMMON_JS.config.$modal;

//         // $.ajax({
//         //     url: "/",
//         //     type: "POST",
//         //     dataType: "json",
//         //     data: "",
//         //     success: function () {

//         //     }
//         // })

//     },
//     remove: function () {

//     },
//     update: function () {

//     }
// }

COMMON_JS.modals = function () {
    var $discountModal = COMMON_JS.config.$discountModal
        , $loginModal = COMMON_JS.config.$loginModal
        , $forgotPasswordModal = COMMON_JS.config.$forgotPasswordModal
        , $registrationModal = COMMON_JS.config.$registrationModal;


    $loginModal.on("click", "a[data-toggle='form']", function (event) {
        event.preventDefault();

        var targetID;
        targetID = $(this).attr("href");
        var relatedTargetHtml = $(this).parents(".modal-content").html();
        var targetHtml = $(targetID).find(".modal-content").html();

        $(this).parents(".modal-content").empty().append(targetHtml);

        $("button[data-dismiss='modal']").click(function () {
            $(this).parents(".modal-content").empty().append(relatedTargetHtml);
        });

    });
}

COMMON_JS.catalogView = function () {
    var $catalogProductsView = COMMON_JS.config.$catalogProductsView
        , $catalogGrid = COMMON_JS.config.$catalogGrid
        , $iconList = $catalogProductsView.children("." + COMMON_JS.config.iconList)
        , $iconTiles = $catalogProductsView.children("." + COMMON_JS.config.iconTiles)
        , $iconPrice = $catalogProductsView.children("." + COMMON_JS.config.iconPrice)
        , $productCard = $catalogGrid.find(COMMON_JS.config.productCard);

    $iconList.click(function () {
        $(this).addClass("active");
        $iconTiles.removeClass("active");
        $catalogGrid.removeClass("tiles").addClass("list");
    })

    $iconTiles.click(function () {
        $(this).addClass("active");
        $iconList.removeClass("active");
        $catalogGrid.removeClass("list").addClass("tiles");
    })

    $iconPrice.click(function () {
        if ($(this).hasClass("icon-up")) {
            $(this).removeClass("icon-up").addClass("icon-down");
        } else {
            $(this).removeClass("icon-down").addClass("icon-up");
        }
    })
}

COMMON_JS.productImages = function () {
    var $productImage = COMMON_JS.config.$productImage,
        $productThumbs = COMMON_JS.config.$productThumbs;

    $productThumbs.find(".slick-slide").eq(0).addClass("choosen");

    $productThumbs.find('[data-src]').click(function (e) { //get thumbs
        e.preventDefault();

        var imgData = $(this).data();
        $productThumbs.find(".slick-slide").removeClass("choosen");
        $(this).parents(".slick-slide").addClass("choosen");
        $productImage.find('img').attr('src', imgData.src).data("zoomImage", imgData.zoomImage);

        //reinit zoom after click on addition images
        $('.zoomContainer').remove();
        $productImage.find("img").elevateZoom({
            zoomType: "inner",
            cursor: "crosshair",
            responsive: true
        });
    })

    $productImage.find("img").elevateZoom({
        zoomType: "inner",
        cursor: "crosshair",
        responsive: true
    });

}

COMMON_JS.sliders = function () {
    var $specialsSlider = COMMON_JS.config.$specialsSlider,
        $featuredSlider = COMMON_JS.config.$featuredSlider,
        $productThumbs = COMMON_JS.config.$productThumbs;

    var specialsConfigSlider = {
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        responsive: [
            {
                breakpoint: 1199,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 680,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    }

    var featuredConfigSlider = {
        slidesToShow: 5,
        slidesToScroll: 1,
        arrows: true,
        responsive: [{
            breakpoint: 1199,
            settings: {
                slidesToShow: 4
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 680,
            settings: {
                slidesToShow: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1
            }
        }
        ]
    }

    var productThumbsConfigSlider = {
        slidesToShow: 5,
        slidesToScroll: 1,
        arrows: true,
        vertical: true,
        infinite: false,
        responsive: [{
            breakpoint: 1199,
            settings: {
                slidesToShow: 4
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 4,
                vertical: false,
            }
        },
        {
            breakpoint: 479,
            settings: {
                slidesToShow: 3,
                vertical: false,
            }
        }
        ]
    }

    $productThumbs.on('init', function (event, slick, direction) {
        if (slick.options.vertical) {
            slick.$slider.removeClass("slider_horizontal").addClass("slider_vertical");
        } else {
            slick.$slider.addClass("slider_horizontal").removeClass("slider_vertical");
        }
    });
    $productThumbs.on('breakpoint', function (event, slick, direction) {
        if (slick.options.vertical) {
            slick.$slider.removeClass("slider_horizontal").addClass("slider_vertical");
        } else {
            slick.$slider.addClass("slider_horizontal").removeClass("slider_vertical");
        }
    });
    $featuredSlider.slick(featuredConfigSlider);
    $specialsSlider.slick(specialsConfigSlider);
    $productThumbs.slick(productThumbsConfigSlider);
}

COMMON_JS.init();
